<?php

/**
 * Implements hook_form_BASE_FORM_ID_alter();
 *
 * Fake to be system database configuration form
 * See http://drupal.org/node/1153646 for more info
 */
function system_form_install_settings_form_alter(&$form, &$form_state, $form_id) {

  $driver = @array_shift($form['settings']['driver']);
  $f['create_database'] = array(
    '#type' => 'checkbox',
    '#title' => st('Create database'),
    '#description' => st("If database does not exists, create it."),
  );
  array_unshift($form['settings'], $f);
  array_unshift($form['settings'], $driver);

  array_unshift($form['#validate'], 'dbcreate_install_settings_form_create_database_validate');

}

function dbcreate_install_settings_form_create_database_validate($form, &$form_state) {

  if ($form_state['values']['create_database']) {
    $db_type = $form_state['values']['driver'];
    if ($db_type == 'sqlite')
      return;

    $db_name = $form_state['values'][$db_type]['database'];
    $db_host = $form_state['values'][$db_type]['host'];
    $db_user = $form_state['values'][$db_type]['username'];
    $db_pass = $form_state['values'][$db_type]['password'];
    $db_port = $form_state['values'][$db_type]['port'];

    $db_path = $db_name;  // MySQL-specific

    _dbcreate_create_database($db_type, $db_user, $db_pass, $db_host, $db_port, $db_path);
  }

}

function _dbcreate_create_database($db_type, $db_user, $db_pass, $db_host, $db_port, $db_path) {

  $function = '_dbcreate_install_create_db_' . $db_type;
  if (function_exists($function))
    return $function($db_user, $db_pass, $db_user, $db_pass, $db_host, $db_port, $db_path);
  else
    form_set_error('driver', $db_type . ': Create database feature is not suported for this database type yet.');

  return TRUE;

}

function _dbcreate_install_create_db_mysql($db_master_user, $db_master_pass, $db_user, $db_pass, $db_host, $db_port, $db_path) {
  // Verify database connection
  $connect_host = ($db_host ? $db_host : 'localhost') . ($db_port ? ":$db_port" : '');
  $conn = @mysql_connect($connect_host, $db_master_user, $db_master_pass);
  if (!$conn) {
    form_set_error('', st('Failed to connect to your MySQL database server by master. MySQL reports the following message:') . ' ' . mysql_error());
    return FALSE;
  }

  // Get version of database
  $result = @mysql_query("SHOW variables WHERE Variable_name='version'");
  if (!$result) {
    form_set_error('', st('Failed get version of database. MySQL reports the following message: ' . mysql_error()));
    mysql_close($conn);
    return FALSE;
  }

  $row = mysql_fetch_array($result);
  $version = $row['Value'];
  if (!$version) {
    form_set_error('', st('Failed to get version of database'));
    mysql_close($conn);
    return FALSE;
  }

  // Create databse
  if (strcmp($version, "4.1") >= 0)
    $utf8_suffix = " DEFAULT CHARACTER SET 'utf8' DEFAULT COLLATE 'utf8_general_ci'";
  $result = @mysql_query(sprintf("CREATE DATABASE IF NOT EXISTS `%s`" . $utf8_suffix, mysql_escape_string($db_path)));
  if (!$result) {
    form_set_error('', st('Failed to create database. MySQL reports the following message: ' . mysql_error()));
    mysql_close($conn);
    return FALSE;
  }

  // Grant permissions
  $result = @mysql_query(sprintf("GRANT ALL ON `%s`.* TO `%s`@'%%' IDENTIFIED BY '%s'",
    mysql_escape_string($db_path), mysql_escape_string($db_user), mysql_escape_string($db_pass)));
  if (!$result) {
    form_set_error('', st('Failed to grant permissions. MySQL reports the following message: ' . mysql_error()));
    mysql_close($conn);
    return FALSE;
  }

  mysql_close($conn);
  return TRUE;
}

function _dbcreate_install_create_db_pgsql($db_master_user, $db_master_pass, $db_user, $db_pass, $db_host, $db_port, $db_path) {
  $conn_string = '';

  // Make connection string
  if (!empty($db_master_user)) {
    $conn_string .= ' user=' . $db_master_user;
  }
  if (!empty($db_master_pass)) {
    $conn_string .= ' password=' . $db_master_pass;
  }
  if (!empty($db_host)) {
    $conn_string .= ' host=' . $db_host;
  }
  if (!empty($db_path)) {
    $conn_string .= ' dbname=postgres';
  }
  if (!empty($db_port)) {
    $conn_string .= ' port=' . $db_port;
  }

  // Connect to the database.
  $conn = @pg_connect($conn_string);
  if (!$conn) {
    drupal_set_message(st('Failed to connect to your PostgreSQL database server. PostgreSQL reports the following message: %error.<ul><li>Are you sure you have the correct username and password?</li><li>Are you sure that you have typed the correct database hostname?</li><li>Are you sure that the database server is running?</li><li>Are you sure you typed the correct database name?</li></ul>For more help, see the <a href="http://drupal.org/node/258">Installation and upgrading handbook</a>. If you are unsure what these terms mean you should probably contact your hosting provider.', array('%error' => 'Connection failed. See log file for failure reason')), 'error');
    return FALSE;
  }

  // Verify if database exists
  $result = @pg_query($conn, sprintf("SELECT datname FROM pg_database WHERE datname='%s'", pg_escape_string($db_path)));
  if (!$result) {
    form_set_error('', st('Failed to select from pg_database. PostgreSQL reports the following message: ' . pg_last_error($conn)));
    pg_close($conn);
    return FALSE;
  }

  if (!pg_fetch_row($result)) {
    // Create database
    $result = @pg_query($conn, sprintf("CREATE DATABASE %s WITH ENCODING 'UNICODE'", pg_escape_string($db_path)));
    if (!$result) {
      form_set_error('', st('Failed to create database. PostgreSQL reports the following message: ' . pg_last_error($conn)));
      pg_close($conn);
      return FALSE;
    }
  }

  // Verify if user exists
  $result = @pg_query($conn, sprintf("SELECT usename FROM pg_user WHERE usename='%s'", pg_escape_string($db_user)));
  if (!$result) {
    form_set_error('', st('Failed to select from pg_user. PostgreSQL reports the following message: ' . pg_last_error($conn)));
    pg_close($conn);
    return FALSE;
  }

  if (!pg_fetch_row($result)) {
    // Create user
    $result = @pg_query($conn, sprintf("CREATE USER %s PASSWORD '%s'", pg_escape_string($db_user), pg_escape_string($db_pass)));
    if (!$result) {
      form_set_error('', st('Failed to create user. PostgreSQL reports the following message:' . ' ' . pg_last_error($conn)));
      pg_close($conn);
      return FALSE;
    }
  }

  // Grant privileges
  $result = @pg_query($conn, sprintf("GRANT ALL ON DATABASE %s TO %s", pg_escape_string($db_path), pg_escape_string($db_user)));
  if (!$result) {
    form_set_error('', st('Failed to grant privileges. PostgreSQL reports the following message: ' . pg_last_error($conn)));
    pg_close($conn);
    return FALSE;
  }

  pg_close($conn);
  return TRUE;
}
